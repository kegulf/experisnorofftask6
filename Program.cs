﻿using System;

/* Task 6: BMI Calculator
 * Write a program that allow the user to input two values:
 * - Weight
 * - Height
 * 
 * It must calculate BMI and then categorize the result:
 * - Underweight: BMI is less than 18.5
 * - Normal weight: BMI is 18.5 to 24.9
 * - Overweight: BMI is 25 to 29.9
 * - Obese: BMI is 30 or more
 */

namespace ExperisNoroffTask6 {
    class Program {

        /// <summary>
        ///     Ask user for weight and height, calculates the BMI,
        ///     then it prints a sentence that tells the user if he/she is
        ///     underweight, normal weight, overweight or obese compared to their height.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {

            var (weight, height) = GetUserInputs();

            double bmi = (double) CalculateBMI(weight, height);

            if(bmi < 18.5) {
                Console.WriteLine("Dude, you seriously need to eat more, you quite underweight.");
            }

            else if (bmi >= 18.5 && bmi < 25) {
                Console.WriteLine("Sweet, you have a normal weight for your height!");
            }

            else if (bmi >= 25 && bmi < 30) {
                Console.WriteLine("You are Overweight. Maybe skip the second serving next time.");
            }

            else if (bmi >= 30) {
                Console.WriteLine("You are obese. Time for a diet?");
            }
        }


        static double CalculateBMI(double weight, double height) {
            return weight /  Math.Pow(height / 100, 2);
        }


        /// <summary>
        ///     Asks the user to input height (cm) and weight (kg).
        ///     Returns a tuple containing (weight, height).
        /// </summary>
        /// <returns>A tuple containing (weight, height)</returns>
        static (double,double) GetUserInputs() {
            Console.WriteLine("Please enter your weight(kg): ");
            double weight = double.Parse(Console.ReadLine());

            Console.WriteLine("Please enter your height (cm): ");
            double height = double.Parse(Console.ReadLine());

            return (weight, height);
        }
    }
}
